class CreateTrades < ActiveRecord::Migration[6.0]
  def change
    create_table :trades do |t|
      t.boolean :market_maker, default: false
      t.float :price
      t.float :quantity
      t.string :symbol
      t.datetime :trade_at
      t.string :pair

      t.timestamps
    end
  end
end
