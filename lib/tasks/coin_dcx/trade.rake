namespace :coindcx do
  namespace :trades do

    desc 'Fetch Trade from CoinDCX'
    # rake coindcx:trades:fetch
    task fetch: :environment do
      require 'coin_dcx/trade_event'
      EM.run CoinDCX::TradeEvent
    end
  end
end
