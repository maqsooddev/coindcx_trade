module CoinDCX
  Config = {
    socket_url: 'wss://stream.coindcx.com/socket.io/?transport=websocket',
    pair_url: 'https://api.coindcx.com/exchange/v1/markets_details'
  }
end
