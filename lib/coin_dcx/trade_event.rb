module CoinDCX
  class TradeEvent

    EVENT = [:open, :message, :close]
    CONFIG = {
      socket_url: 'wss://stream.coindcx.com/socket.io/?transport=websocket',
      pair_url: 'https://api.coindcx.com/exchange/v1/markets_details',
      pairs: ['I-BTC_INR', 'B-EOS_USDT']
    }

    class << self
      def call(env = nil); new(env); end
    end

    attr_reader :option

    def initialize(option = nil)
      @option = option || CONFIG
      bind
    end

    def connection
      @connection ||= Faye::WebSocket::Client.new(socket_url)
    end

    def socket_url
      @socket_url ||= option.fetch(:socket_url) { raise 'SOCKET_URL not found' }
    end

    def pair_url
      @pair_url ||= option.fetch(:pair_url) { raise 'PAIR_URL not found' }
    end

    def pairs
      @paris ||= begin
        paris = JSON.parse(Net::HTTP.get(URI.parse(pair_url))).pluck("pair")
        option[:pairs] == :all ? pairs : (paris & option[:pairs])
      end
    end

    def message_log(response)
      log response_parser(response)
    end

    private

    def open(response)
      pairs.each do |pair|
        connection.send("42#{['join', {channelName: pair}].to_json}")
      end
    end

    def message(response)
      event, data = response_parser(response)
      if event == 'new-trade'
        CoinDCX::TradeJob.perform_later(data)
      end
    end

    def close(response)
      self.class.call(option)
    end

    def response_parser(response)
      response.data&.match(/\A\d*(.*)\z/)
      event, data = JSON.parse($1).last.values_at('event', 'data') rescue []
      data  = JSON.parse(data) rescue {}
      [event, data]
    end

    def event_mapper(event)
      connection.on(event) do |response|
        log_mapper(event, response)
        send(event, response)
      end
    end

    def log_mapper(event, response)
      event_log = :"#{event}_log"
      if respond_to? event_log
        send(event_log, response)
      else
        log("#{event} Event occured")
      end
    end

    def log(msg)
      puts msg
    end

    def bind
      EVENT.each { |event| event_mapper(event) }
    end
  end
end
