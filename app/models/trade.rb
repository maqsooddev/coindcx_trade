class Trade < ApplicationRecord
  validates :price, :quantity, :symbol, :trade_at, :pair, presence: true
end
