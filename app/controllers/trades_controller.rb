class TradesController < ApplicationController
  def index
    render json: Trade.order(trade_at: :desc).limit(20) if request.xhr?
  end
end
