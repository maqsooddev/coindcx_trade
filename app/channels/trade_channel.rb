class TradeChannel < ApplicationCable::Channel
  def subscribed
    stop_all_streams
    stream_from "trade_channel"
  end

  def unsubscribed
    stop_all_streams
  end
end
