import consumer from "./consumer"
import $ from 'jquery'

var scriptjs = require('scriptjs');

scriptjs('https://canvasjs.com/assets/script/canvasjs.min.js', function() {
  window.onload = function () {

    var dataPoints = [];

    var chart = new CanvasJS.Chart("chartContainer", {
      animationEnabled: true,
      theme: "light2", // "light1", "light2", "dark1", "dark2"
      exportEnabled: true,
      title: {
        text: "CoinDCX"
      },
      subtitles: [{
        text: "New Trades"
      }],
      axisX: {
        interval: 1,
        valueFormatString: "MMM"
      },
      axisY: {
        includeZero: false,
        prefix: "$",
        title: "Price"
      },
      toolTip: {
        content: "Date: {x}<br /><strong>Price:</strong><br />Open: {y[0]}, Close: {y[3]}<br />High: {y[1]}, Low: {y[2]}"
      },
      data: [{
        type: "candlestick",
        yValueFormatString: "$##0.00",
        dataPoints: dataPoints
      }]
    });

    $.get(window.location.href, getDataPointsFromCSV);

    function getDataPointsFromCSV(csv) {
      var csvLines  = [];
      var points = [];
      csvLines = csv
      for (var i = 0; i < csvLines.length; i++) {
        if (csvLines[i] != undefined ) {
          points = csvLines[i]
          insertTrade(points)
        }
      }
      chart.render();
    }

    var insertTrade = function (trade) {
      dataPoints.push({
        x: new Date(trade['trade_at']),
        y: [0,0, trade['quantity'], trade['price']]
      });
    }
    var dataLength = 20;
    var updateChart = function (trade) {
      insertTrade(trade)

      if (dataPoints.length > dataLength) {
        dataPoints.shift();
      }
      chart.render();
    };
    consumer.subscriptions.create("TradeChannel", {
      received(response) {
        updateChart(response.data)
      }
    });
  }
});

