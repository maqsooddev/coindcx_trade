module CoinDCX
  class TradeJob < ApplicationJob
    def perform(params)
      if trade = Trade.create(params_mapper(params))
        ActionCable.server.broadcast("trade_channel", data: trade.as_json)
      end
    end

    def params_mapper(params)
      {
        market_maker: params['m'],
        price:        params['p'],
        quantity:     params['q'],
        trade_at:     DateTime.strptime(params['T'].to_s, '%Q'),
        symbol:       params['s'],
        pair:         params['channel'],
      }
    end
  end
end
