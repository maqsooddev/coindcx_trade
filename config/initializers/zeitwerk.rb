Rails.autoloaders.each do |autoloader|
  autoloader.inflector.inflect(
    'coin_dcx'  => 'CoinDCX'
  )
end
